import React, { Component, PropTypes } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {Overlay} from 'react-native-elements'
import {Text,Image, ScrollView,KeyboardAvoidingView,View,TouchableOpacity,Dimensions} from 'react-native'
import SearchableDropdown from 'react-native-searchable-dropdown';

const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0

var {height, width} = Dimensions.get('window');


export default class MapInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            size: 160,
        };
    }
    selectionOnPress() {
        this.setState({ size:300 });
        setTimeout(() => {this.setState({size:160})}, 4000)
        
     }

    
    render() {
        return (
           
 
             
            
            <GooglePlacesAutocomplete
             
                placeholder='Search'
                //onFocus={()=>this.selectionOnPress()}
                minLength={2} // minimum length of text to search
                autoFocus={true}
                returnKeyType={'search'} // Can be left out for default return key 
                listViewDisplayed='auto'  // true/false/undefined
                fetchDetails={true}
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    this.props.notifyChange(details.geometry.location);
                   

                }
                }
                
                styles={{
                    container:{

                        marginTop:10,
                        marginLeft: 30,
                        //marginRight:20,
                        //backgroundColor: 'white',//'rgba(0,0,0,0)',
                        borderWidth:0,
                        borderTopWidth: 0,
                        borderBottomWidth:0,
                        borderTopLeftRadius:5,
                        borderTopRightRadius:5,
                        width: 335,
                        height:54,
                        borderColor:'white',
                        marginBottom:5,
                    },
                    textInputContainer: {
                    // width: '335',
                    // height:'54',
                    borderRadius:5,
                    backgroundColor:'white',
                    },
                    listView:{

                    },
                   
                    predefinedPlacesDescription: {
                    color: '#1faadb'
                    }
                }}

                query={{
                    key: 'AIzaSyC-5CY9mOCeg5Y3IhPqi_Yd0-DZtWrJl-E',
                    language: 'en'
                }}

                nearbyPlacesAPI='GooglePlacesSearch'
                debounce={300}
                renderLeftButton={()  => <Image source={require('../assets/map_pointer.png')}
                                                style={{
                                                    marginTop:15,
                                                    marginLeft:20,
                                                    //marginRight:20,
                                                }} />}
                renderRightButton={() => <Image source={require('../assets/Path.png')}
                                                style={{
                                                marginTop:15,
                                                //marginLeft:20,
                                                marginRight:41,
                                            }} />
                                            }
               
            />
            


       
         

           
          
           
        );
    }
}
