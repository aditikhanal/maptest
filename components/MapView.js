import React from 'react';
import MapView from 'react-native-maps';

const MyMapView = (props) => {
    return (
        <MapView
            style={{ flex: 1 }}
            region={props.region}
            showsUserLocation={true}
            followsUserLocation={true}
            //onRegionChange={(reg) => props.onRegionChange(reg)}
            >
       </MapView>
    )
}
export default MyMapView;