import MapView, { AnimatedRegion } from 'react-native-maps';
import React, {Component} from "react";
import { Text, View, StyleSheet } from 'react-native';
//import Constants from 'expo-constants';
import MapContainer from './containers/MapContainer';
import MapInput from './components/MapInput'
import { PermissionsAndroid } from 'react-native';



async function requestLocationPermission() 
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': "Urgent Hero",
          'message': 'Urgent Hero wants to access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
        
      } else {
        console.log("location permission denied")
        alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
    }
  }

export default class App extends React.Component {
    async componentWillMount() {
        await requestLocationPermission()
        }
     
  render() {
    return (
      <View style={styles.container
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: 'pink',
       //width: 500,
      //height: 500,
    }>
     
       
        <MapContainer /> 
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    //paddingTop: Constants.statusBarHeight
  }
});

