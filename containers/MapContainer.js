import React from 'react';
import { View,Text,TouchableOpacity,Image,ScrollView } from 'react-native';
import MapInput from '../components/MapInput';
import MyMapView from '../components/MapView';
import {Overlay} from 'react-native-elements';
import { getLocation, geocodeLocationByName } from '../services/location-service';

class MapContainer extends React.Component {
    state = {
        region: {
            latitude: 27.6881,
            longitude: 85.3492,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        size:160
    };
    selectionOnPress() {
        this.setState({ size:300 });
        console.log("button pressed")
        setTimeout(() => {this.setState({ size:160})}, 3000)
        
     }


    componentDidMount() {
        this.getInitialState();
    }
    getInitialState() {
        getLocation().then(
            (data) => {
                console.log(data);
                this.setState({
                    region: {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                });
            }
        );
    }
    getCoordsFromName(loc) {
        this.setState({
            region: {
                latitude: loc.lat,
                longitude: loc.lng,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003
            }
        });
    }
    onMapRegionChange(region) {
        this.setState({ region });
    }

    render() {
        return (
            
            <View style={{
                //direction:"column",
                flex: 1,
                //justifyContent: 'center',
                //alignItems: 'center',
                //backgroundColor: this.state.color,
               //width: 500,
              //height: 500,
            }}>
                 
            <View style={{
                //flex: 1,
                alignSelf: "stretch",
                backgroundColor: "#10b4f2",
                width: 500,
                height: this.state.size,
                marginBottom:0,
                
            }}>
                <View style={{ flexDirection: "row", paddingTop: 5, paddingLeft: 5 }}>
                <TouchableOpacity
                            onPress={() => null}>

                            <Image source={require('../assets/PathPathPath161612128LPage_11_copy.png')} style={{marginLeft:26,marginTop:25}} />

                        </TouchableOpacity>
                 
                 
                          <Text style={{
                    fontFamily: 'ProximaNova-Regular',
                    fontSize: 18,
                    lineHeight: 22,
                    fontWeight: '600',
                    color: '#fff',
                    marginLeft:51,
                    marginRight:40,
                    marginTop: 20,
                   
                }}>Emergency Location (2/4)</Text>

                 <TouchableOpacity
                            onPress={() => null}>

                            <Image source={require('../assets/notification.png')} style={{ height: 24, width: 21,marginLeft:7,marginTop:20}} />

                        </TouchableOpacity>
                       </View>
               


                
       <Text style={{
                    fontFamily: 'ProximaNova-Regular',
                    fontSize: 15,
                    lineHeight: 18,
                    fontWeight: '600',
                    color: '#fff',
                    marginLeft: 40,
                    marginTop: 20,

                }}>You are just one click away to book your Hero!</Text>


                
                    
            <MapInput notifyChange={(loc) => this.getCoordsFromName(loc)}/> 
            </View>
           { this.state.region['latitude'] ?
                         <View style={{     //position: 'absolute',
                                            alignSelf:'stretch',
                                            //flex:1,
                                            //marginTop:300,
                                            marginBottom:0,
                                            borderTopLeftRadius:5,
                                            borderTopRightRadius:5,
                                            top:0,
                                            left: 0,
                                            right: 0,
                                            bottom: 0, 
                    flex:1}}>
                            <MyMapView
                                region={this.state.region}
                                onRegionChange={(reg) => this.onMapRegionChange(reg)} />
                                  <TouchableOpacity onPress={()=>this.selectionOnPress()} style={{
                                               //position: "relative",
                                               width: 335,
                                               height: 56,
                                               marginLeft:40,
                                               backgroundColor: '#10b4f2',
                                               marginBottom:5,
                                               marginTop:3,
                                               overflow: "hidden",
                                           }}>
                                             <Text style={{
                                   fontFamily: 'ProximaNova-Regular',
                                   fontSize: 16,
                                   lineHeight: 20,
                                   fontWeight: '700',
                                   color: '#fff',
                                   marginLeft: 75,
                                   marginTop:20,
                                 
                                 
                           
                                 
                               }}>FIND YOUR HERO NOW</Text>
                                       </TouchableOpacity>


                        </View> : null}
                        
                      
                                       
                        

   

                        
            </View>
            
                       
        );
    }
}

export default MapContainer;